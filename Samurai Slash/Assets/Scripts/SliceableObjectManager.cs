using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliceableObjectManager : MonoBehaviour
{
    
    [Header("Game parameters")] 
    
    [Tooltip("Countdown until sliced pieces are removed, negative if slices are to remain permanently")] 
    [SerializeField]
    private float slicedPartsRemovalTime;
    
    [Tooltip("Countdown until whole objects are removed, negative if objects are to remain permanently")] 
    [SerializeField]
    private float wholeObjectRemovalTime;
    
    


    public void OnObjectSliced(GameObject[] slices)
    {
        if (slicedPartsRemovalTime >= 0)
        {
            foreach (var slice in slices)
            {
                if (slice != null)
                {
                    Destroy(slice, slicedPartsRemovalTime);
                }
            }
        }
    }

    
    public void OnObjectSpawned(GameObject wholeObject)
    {
        if (wholeObjectRemovalTime >= 0)
        {
            if (wholeObject != null)
            {
                Destroy(wholeObject, wholeObjectRemovalTime);
            }
        }
    }
}
