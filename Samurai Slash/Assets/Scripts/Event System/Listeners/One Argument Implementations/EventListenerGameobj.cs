using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Event listener for events having one argument: a gameobject
/// </summary>
public class EventListenerGameobj : EventListenerOneArg<GameObject>
{
    
}
