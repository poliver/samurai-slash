using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Event listener for events having one argument: a bool
/// </summary>
public class EventListenerBool : EventListenerOneArg<bool>
{
    
}
