using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Event listener listening to events with no argument
/// </summary>
public class EventListenerNoArg : MonoBehaviour
{
    [SerializeField] private EventChannelNoArgSO eventChannelNoArg;
    [SerializeField] private UnityEvent onEventRaised;

    private void OnEnable()
    {
        eventChannelNoArg.Event += RaiseEvent;
    }

    private void OnDisable()
    {
        eventChannelNoArg.Event -= RaiseEvent;
    }

    private void RaiseEvent()
    {
        onEventRaised.Invoke();
    }
}
