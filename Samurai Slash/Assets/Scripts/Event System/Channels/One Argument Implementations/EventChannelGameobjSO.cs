using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Channel for events having one argument: a Gameobject
/// </summary>

[CreateAssetMenu(fileName = "EventChannelGameObj", menuName = "Events/Event Channel Gameobject")]
public class EventChannelGameobjSO : EventChannelOneArgSO<GameObject>
{
    
}
