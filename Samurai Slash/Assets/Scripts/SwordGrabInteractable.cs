using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


/// <summary>
/// An XRGrabInteractable customized for the sword
/// </summary>
public class SwordGrabInteractable : XRGrabInteractable
{
   [Header("Sword Specific")]
   [SerializeField] private EventChannelNoArgSO swordGrabbedChannel;



   protected override void OnSelectEntered(SelectEnterEventArgs args)
   {
      base.OnSelectEntered(args);  
      swordGrabbedChannel.Event.Invoke();
   }


   protected override void OnSelectExiting(SelectExitEventArgs args)
   {
      //Do nothing
   }

   protected override void OnSelectExited(SelectExitEventArgs args)
   {
      //Do nothing
   }
}
