using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{

    [SerializeField] private GameScoreTrackerSO gameScoreTracker;


    private void Start()
    {
        gameScoreTracker.timesPlayerHitByObjects = 0;
        gameScoreTracker.objectsSliced = 0;
        
    }

    
    /// <param name="gameObject">The object that hit the player</param>
    public void OnPlayerHit(GameObject gameObject)
    {
        gameScoreTracker.timesPlayerHitByObjects++;
        Debug.Log("Player hit by " + gameScoreTracker.timesPlayerHitByObjects);
    }

   
    /// <param name="gameObjects">The sliced parts of the object</param>
    public void OnObjectSliced(GameObject[] gameObjects)
    {
        gameScoreTracker.objectsSliced++;
        Debug.Log("Sliced: " + gameScoreTracker.objectsSliced);
    }

    
}
