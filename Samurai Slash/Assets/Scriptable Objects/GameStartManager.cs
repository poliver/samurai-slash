using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStartManager : MonoBehaviour
{
    [SerializeField] private EventChannelOneArgSO<bool> toggleShootingChannel;

    [Header("Countdown")]
    [SerializeField] private EventChannelOneArgSO<int> startGameCountdownChannel;
    [SerializeField] private int secondsCountdown;
    
    private int secondsRemaining;
    private readonly WaitForSeconds coroutineInterval = new WaitForSeconds(1);


    private void Start()
    {
        secondsRemaining = secondsCountdown;
    }
    

    public void StartCountdown()
    {
        StartCoroutine(Countdown());
    }
    
    private IEnumerator Countdown()
    {
        while (secondsRemaining != 0)
        {
            startGameCountdownChannel.Event.Invoke(secondsRemaining);
            secondsRemaining--;
            yield return coroutineInterval;
        }
        startGameCountdownChannel.Event.Invoke(secondsRemaining); // to also raise event for 0
        StartGameplay();
    }

    private void StartGameplay()
    {
        toggleShootingChannel.Event.Invoke(true);
    }
    
}
