using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StartingUIManager : MonoBehaviour
{

    [SerializeField] private GameObject startingCanvas;
    
    [Header("Countdown")]
    [SerializeField] private TextMeshProUGUI countdownText;
    
    

    /// <summary>
    /// To be called when the game is ready to start, shows informative text and countdown
    /// </summary>
    private void OnGameplayStart()
    {
        startingCanvas.SetActive(true);
    }

    public void UpdateCountdownUI(int timeLeft)
    {
        
        if (!startingCanvas.activeInHierarchy)
        {
            startingCanvas.SetActive(true);
        }
        
        countdownText.text = timeLeft.ToString();
        if (timeLeft == 0)
        {
            startingCanvas.SetActive(false);
        }
    }
    
}
