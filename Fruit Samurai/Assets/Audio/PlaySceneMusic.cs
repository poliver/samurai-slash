using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySceneMusic : MonoBehaviour
{
    
    [Header("Sound Effects")] 
    [SerializeField]
    private AudioCueEventChannelSO MusicEvent_Channel;
    [SerializeField]
    private AudioCueSO audioCue;
    [SerializeField]
    private AudioConfigurationSO audioConfiguration;
    
    // Start is called before the first frame update
    void Start()
    {
       PlayMusic();
    }

    private void PlayMusic()
    {
        MusicEvent_Channel.OnAudioCueRequested.Invoke(audioCue, audioConfiguration, transform.position);
    }

    
}
