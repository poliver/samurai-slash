using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Makes an object sliceable when collided with colliders of a certain layer
/// </summary>
public class SliceableObject : MonoBehaviour
{
    [SerializeField] private EventChannelGameobjArraySO objectSlicedEventChannel;
    
    [Tooltip("The info on this object's sliced and unsliced gameobjects")]
    [SerializeField] private SliceableObjectSO thisObject;
    [Tooltip("The gameobjects on these layers will be able to slice this object on collision")]
    [SerializeField] private LayerMask interactionLayers;

    [Header("Sound Effects")] 
    [SerializeField]
    private AudioCueEventChannelSO SFXEvent_Channel;
    [SerializeField]
    private AudioCueSO audioCue;
    [SerializeField]
    private AudioConfigurationSO audioConfiguration;
    
    
    

    private void OnCollisionEnter(Collision other)
    {
        //Check if the object layer is not within the interaction layers
        if ((interactionLayers.value & (1 << other.gameObject.layer)) == 0)
        {
            return;
        }
        GameObject[] instantiatedSlices = InstantiateSlices();
        objectSlicedEventChannel.Event.Invoke(instantiatedSlices);
        PlaySoundEffect();
        Destroy(gameObject);
    }


    private GameObject[] InstantiateSlices()
    {
        GameObject[] slicedObjectModel = thisObject.objectSliced;
        GameObject[] slicedObjectInstantiated = new GameObject[slicedObjectModel.Length];
        for (int i = 0; i < slicedObjectModel.Length; ++i)
        {
            GameObject slice = slicedObjectModel[i];
            slicedObjectInstantiated[i] = Instantiate(slice, transform.position + slice.transform.position,
                transform.rotation * slice.transform.rotation);
        }

        return slicedObjectInstantiated;
    }

    private void PlaySoundEffect()
    {
        if (audioCue != null && audioConfiguration != null)
        {
            SFXEvent_Channel.OnAudioCueRequested.Invoke(audioCue, audioConfiguration, transform.position);
        }
    }
}
