using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This SO is used as the base class for objects which can be cut
/// </summary>

[CreateAssetMenu(menuName = "Gameplay/Sliceable Object")]
public class SliceableObjectSO : ScriptableObject
{
    [Tooltip("Object before slice")]
    public GameObject objectWhole;

    [Tooltip("Object(s) after slice")]
    public GameObject[] objectSliced;
    
}
