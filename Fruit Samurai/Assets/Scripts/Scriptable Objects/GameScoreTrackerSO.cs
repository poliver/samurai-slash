using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Gameplay/Score Tracker")]
public class GameScoreTrackerSO : ScriptableObject
{
    public int objectsSliced;
    public int timesPlayerHitByObjects;
}
