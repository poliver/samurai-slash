using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/Shooter Parameters")]
public class ShooterParamsSO : ScriptableObject
{
    public SliceableObjectSO[] projectiles;
    public float projectileSpeed;
    public float minTimeBetweenShots;
    public float maxTimeBetweenShots;
    [Tooltip("Max spawn distance in local X axis from local origin")]
    public float maxSpawnDistX;
    [Tooltip("Max spawn distance in local Y axis from local origin")]
    public float maxSpawnDistY;
}
