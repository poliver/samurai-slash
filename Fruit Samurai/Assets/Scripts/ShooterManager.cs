using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterManager : MonoBehaviour
{
    [SerializeField] private float distanceFromPlayer;
    [SerializeField] private ProjectileShooter shooter;
    [SerializeField] private Transform playerHead;

    [Tooltip("The shooter will shoot around the specified height. Here 0 means shooting at floor level while 1 means at head level")]
    [Range(0, 1.5f)] [SerializeField] private float shootWhere = 0.8f;
    
    private Transform shooterTransform;
    private Vector3 shooterForwardDirection;
    

    private void Start()
    {

        shooterTransform = shooter.transform;
        shooterForwardDirection = shooterTransform.forward;
    }

    private void Update()
    {
        Vector3 shooterPos = playerHead.position;
        shooterPos.y = playerHead.position.y - ((1 - shootWhere) * playerHead.localPosition.y);
        shooterTransform.position = shooterPos - shooterForwardDirection  * distanceFromPlayer;
        shooterTransform.LookAt(playerHead);
    }

    public void ToggleFire(bool value)
    {
        shooter.ToggleFire(value);
    }
}
