using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/// <summary>
/// Handles the player's collider to give it the right size and position at all times.
/// </summary>
public class PlayerHitbox : MonoBehaviour
{
    [SerializeField] private Transform playerHead; //Will use its position as the top of the collider
    [SerializeField] private CapsuleCollider playerHitbox;
    
    private Vector3 feetPos = Vector3.zero;


    private void Start()
    {
        playerHitbox.center = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        
        //Update collider size
        float headHeight = playerHead.localPosition.y;
        playerHitbox.height = headHeight;
        float midHeight = (feetPos.y + headHeight) / 2f;
        Vector3 colliderNewPos = playerHead.transform.localPosition;
        colliderNewPos.y = midHeight;
        playerHitbox.transform.localPosition = colliderNewPos;
        
    }
}
