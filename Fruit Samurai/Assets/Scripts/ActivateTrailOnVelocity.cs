using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Activates and deactivates trail based on object velocity
/// </summary>

[RequireComponent(typeof(TrailRenderer))]
public class ActivateTrailOnVelocity : MonoBehaviour
{
    [SerializeField] private Rigidbody rbForVelocity;
    [SerializeField] private float thresholdVelocity;

    private TrailRenderer trailRenderer;
    private void Awake()
    {
        trailRenderer = GetComponent<TrailRenderer>();
    }

    void Update()
    {
        trailRenderer.emitting = rbForVelocity.velocity.magnitude > thresholdVelocity;
    }
}
