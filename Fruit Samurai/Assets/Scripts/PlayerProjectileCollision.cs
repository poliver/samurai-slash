using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectileCollision : MonoBehaviour
{
    
    [Tooltip("The gameobjects on these layers will be able to damage player")]
    [SerializeField] private LayerMask interactionLayers;

    [Tooltip("The channel to communicate that the player has been hit by something")]
    //[SerializeField] private PlayerEventsChannelOneArgSo playerEventsChannelOneArg; @TODO
    
    
    [Header("Sound Effects")] 
    [SerializeField]
    private AudioCueEventChannelSO SFXEvent_Channel;
    [SerializeField]
    private AudioCueSO playerHit;
    [SerializeField]
    private AudioConfigurationSO audioConfiguration;
    
    private void OnCollisionEnter(Collision other)
    {
        //Check if the object layer is not within the interaction layers
        if ((interactionLayers.value & (1 << other.gameObject.layer)) == 0)
        {
            return;
        }
        //playerEventsChannelOneArg.OnPlayerCollision.Invoke(other.gameObject);
        PlaySoundEffect();
        Destroy(other.gameObject);
    }
    
    
    private void PlaySoundEffect()
    {
        if (playerHit!= null && audioConfiguration != null)
        {
            SFXEvent_Channel.OnAudioCueRequested.Invoke(playerHit, audioConfiguration, transform.position);
        }
    }
}
