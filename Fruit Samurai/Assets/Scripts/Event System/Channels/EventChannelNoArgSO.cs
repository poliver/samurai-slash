using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "EventChannelNoArg", menuName = "Events/Event channel with zero arguments")]
public class EventChannelNoArgSO : ScriptableObject
{
    public UnityAction Event;
}
