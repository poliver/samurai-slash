using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class EventChannelOneArgSO<T> : ScriptableObject
{
    public UnityAction<T> Event; 
}
