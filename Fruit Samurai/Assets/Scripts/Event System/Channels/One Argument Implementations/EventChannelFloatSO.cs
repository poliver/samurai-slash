using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Channel for events having one argument: a float
/// </summary>

[CreateAssetMenu(fileName = "EventChannelFloat", menuName = "Events/Event Channel Float")]
public class EventChannelFloatSO : EventChannelOneArgSO<float>
{
   
}
