using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Channel for events having one argument: an array of Gameobjects
/// </summary>

[CreateAssetMenu(fileName = "EventChannelGameObjArray", menuName = "Events/Event Channel Gameobject Array")]
public class EventChannelGameobjArraySO : EventChannelOneArgSO<GameObject[]>
{
    
}
