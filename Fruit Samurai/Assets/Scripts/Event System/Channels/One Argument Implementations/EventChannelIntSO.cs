using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Channel for events having one argument: an int
/// </summary>

[CreateAssetMenu(fileName = "EventChannelInt", menuName = "Events/Event Channel Int")]
public class EventChannelIntSO : EventChannelOneArgSO<int>
{
   
}
