using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Channel for events having one argument: a Bool
/// </summary>

[CreateAssetMenu(fileName = "EventChannelBool", menuName = "Events/Event Channel Bool")]
public class EventChannelBoolSO : EventChannelOneArgSO<bool>
{
    
}

