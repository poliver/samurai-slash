using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Event listener listening to events with one argument
/// </summary>
/// <typeparam name="T">The type of the argument based in the Event</typeparam>
public abstract class EventListenerOneArg<T> : MonoBehaviour
{
   [SerializeField] 
   private EventChannelOneArgSO<T> eventChannelOneArg;
   
   [SerializeField] 
   private UnityEvent<T> onEventRaised;

   private void OnEnable()
   {
      eventChannelOneArg.Event += RaiseEvent;
   }

   private void OnDisable()
   {
      eventChannelOneArg.Event -= RaiseEvent;
   }

   private void RaiseEvent(T arg)
   {
      onEventRaised.Invoke(arg);
   }
}
