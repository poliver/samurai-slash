using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Event listener for events having one argument: an int
/// </summary>
public class EventListenerInt : EventListenerOneArg<int>
{
  
   
}