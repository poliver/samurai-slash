using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Event listener for events having one argument: an array of gameobjects
/// </summary>
public class EventListenerGameobjArray : EventListenerOneArg<GameObject[]>
{
   
}
