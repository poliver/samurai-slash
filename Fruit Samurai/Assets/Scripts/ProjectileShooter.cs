using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Spawns and shoots items in a given direction
/// </summary>
public class ProjectileShooter : MonoBehaviour
{
    [SerializeField] private EventChannelGameobjSO SliceableObjectSpawnedChannel;
    
    [SerializeField] private ShooterParamsSO shooterParams;
    [SerializeField] private Transform shootOrigin;
    [SerializeField] private Transform spawnOrigin;
    [SerializeField] private bool applyGravity;

    private Coroutine firingCoroutine = null;
    
    public void ToggleFire(bool shouldFire)
    {
        if (shouldFire)
        {
            if (firingCoroutine == null)    //To prevent coroutine from starting multiple instances at the same time
            {
                firingCoroutine = StartCoroutine(StartFiring());
            }
        }
        else
        {
            StopCoroutine(firingCoroutine);
            firingCoroutine = null;
        }
    }

    
    private IEnumerator StartFiring()
    {
        while (true)
        {
            FireRandomProjectile();
            yield return new WaitForSeconds(Random.Range(shooterParams.minTimeBetweenShots,
                shooterParams.maxTimeBetweenShots));
        }
    }
    

    /// <summary>
    /// Fires a random object from the list of available objects
    /// </summary>
    public void FireRandomProjectile()
    {
        GameObject projectileModel =
            shooterParams.projectiles[Random.Range(0, shooterParams.projectiles.Length)].objectWhole;
        GameObject projectile = Instantiate(projectileModel, spawnOrigin);

        SliceableObjectSpawnedChannel.Event.Invoke(projectile);
        Rigidbody rb = projectile.GetComponent<Rigidbody>();
        if (rb == null)
        {
            Debug.LogWarning("A projectile prefab is missing a rigidbody");
            return;
        }
        SetGravity(rb, applyGravity);
        SetRandomOrigin(projectile);
        SetVelocity(rb, spawnOrigin.forward,  shooterParams.projectileSpeed);
    }

    private void SetGravity(Rigidbody rb, bool applyGravity)
    {
        rb.useGravity = applyGravity;
    }

    private void SetRandomOrigin(GameObject projectile)
    {
        projectile.transform.localPosition = GetRandomOriginWithinBounds(Vector3.zero, shooterParams.maxSpawnDistX,
            shooterParams.maxSpawnDistY);
        projectile.transform.rotation = Quaternion.identity;
        
    }

    private void SetVelocity(Rigidbody rb, Vector3 direction, float velocity)
    {
        rb.velocity = direction * velocity;
    }

    /// <summary>
    /// Return a random local position within bounds 
    /// </summary>
    /// <param name="origin">The local origin coordinates</param>
    /// <param name="maxSpawnDistX">max possible distance from the origin along X axis </param>
    /// <param name="maxSpawnDistY">max possible distance from the origin along Y axis</param>
    /// <returns></returns>
    private Vector3 GetRandomOriginWithinBounds(Vector3 origin, float maxSpawnDistX, float maxSpawnDistY)
    {
        float randomPosX = origin.x + Random.Range(-maxSpawnDistX, maxSpawnDistX);
        float randomPosY = origin.y + Random.Range(-maxSpawnDistY, maxSpawnDistY);
        return new Vector3(randomPosX, randomPosY, origin.z);

    }
}
