                           T               2020.3.3f1 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                       \       ŕyŻ     `       ô                                                                                                                                            ŕyŻ                                                                                    ProjectileShooter     using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Spawns and shoots items in a given direction
/// </summary>
public class ProjectileShooter : MonoBehaviour
{
    [SerializeField] private EventChannelGameobjSO SliceableObjectSpawnedChannel;
    
    [SerializeField] private ShooterParamsSO shooterParams;
    [SerializeField] private Transform shootOrigin;
    [SerializeField] private Transform spawnOrigin;
    [SerializeField] private bool applyGravity;

    private Coroutine firingCoroutine = null;
    
    public void ToggleFire(bool shouldFire)
    {
        if (shouldFire)
        {
            if (firingCoroutine == null)    //To prevent coroutine from starting multiple instances at the same time
            {
                firingCoroutine = StartCoroutine(StartFiring());
            }
            
        }
        else
        {
            StopCoroutine(firingCoroutine);
            firingCoroutine = null;
        }
    }

    
    private IEnumerator StartFiring()
    {
        while (true)
        {
            FireRandomProjectile();
            yield return new WaitForSeconds(Random.Range(shooterParams.minTimeBetweenShots,
                shooterParams.maxTimeBetweenShots));
        }
    }
    

    /// <summary>
    /// Fires a random object from the list of available objects
    /// </summary>
    public void FireRandomProjectile()
    {
        GameObject projectileModel =
            shooterParams.projectiles[Random.Range(0, shooterParams.projectiles.Length)].objectWhole;
        GameObject projectile = Instantiate(projectileModel, spawnOrigin);

        SliceableObjectSpawnedChannel.Event.Invoke(projectile);
        Rigidbody rb = projectile.GetComponent<Rigidbody>();
        if (rb == null)
        {
            Debug.LogWarning("A projectile prefab is missing a rigidbody");
            return;
        }
        SetGravity(rb, applyGravity);
        SetRandomOrigin(projectile);
        SetVelocity(rb, spawnOrigin.forward,  shooterParams.projectileSpeed);
    }

    private void SetGravity(Rigidbody rb, bool applyGravity)
    {
        rb.useGravity = applyGravity;
    }

    private void SetRandomOrigin(GameObject projectile)
    {
        projectile.transform.localPosition = GetRandomOriginWithinBounds(Vector3.zero, shooterParams.maxSpawnDistX,
            shooterParams.maxSpawnDistY);
        projectile.transform.rotation = Quaternion.identity;
        
    }

    private void SetVelocity(Rigidbody rb, Vector3 direction, float velocity)
    {
        rb.velocity = direction * velocity;
    }

    /// <summary>
    /// Return a random local position within bounds 
    /// </summary>
    /// <param name="origin">The local origin coordinates</param>
    /// <param name="maxSpawnDistX">max possible distance from the origin along X axis </param>
    /// <param name="maxSpawnDistY">max possible distance from the origin along Y axis</param>
    /// <returns></returns>
    private Vector3 GetRandomOriginWithinBounds(Vector3 origin, float maxSpawnDistX, float maxSpawnDistY)
    {
        float randomPosX = origin.x + Random.Range(-maxSpawnDistX, maxSpawnDistX);
        float randomPosY = origin.y + Random.Range(-maxSpawnDistY, maxSpawnDistY);
        return new Vector3(randomPosX, randomPosY, origin.z);

    }
}
                        ProjectileShooter       